defmodule InstrumentStoreWeb.GuitarControllerTest do
  use InstrumentStoreWeb.ConnCase

  @create_attrs %{is_available: true, model: "some model", year: 42}
  @invalid_attrs %{is_available: nil, model: nil, year: nil}

  describe "index" do
    test "lists all guitars", %{conn: conn} do
      conn = get(conn, Routes.guitar_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Guitars"
    end
  end

  describe "new guitar" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.guitar_path(conn, :new))
      assert html_response(conn, 200) =~ "New Guitar"
    end
  end

  describe "create guitar" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.guitar_path(conn, :create), guitar: @create_attrs)
      assert redirected_to(conn) == Routes.guitar_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.guitar_path(conn, :create), guitar: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Guitar"
    end
  end
end
