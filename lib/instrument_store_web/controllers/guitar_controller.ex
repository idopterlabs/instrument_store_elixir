defmodule InstrumentStoreWeb.GuitarController do
  use InstrumentStoreWeb, :controller

  alias InstrumentStore.Instruments
  alias InstrumentStore.Instruments.Guitar

  def index(conn, _params) do
    guitars = Instruments.list_guitars()
    render(conn, "index.html", guitars: guitars)
  end

  def new(conn, _params) do
    changeset = Instruments.change_guitar(%Guitar{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"guitar" => guitar_params}) do
    case Instruments.create_guitar(guitar_params) do
      {:ok, _guitar} ->
        conn
        |> put_flash(:info, "Guitar created successfully.")
        |> redirect(to: Routes.guitar_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    guitar = Instruments.get_guitar!(id)
    render(conn, "show.html", guitar: guitar)
  end

  def delete(conn, %{"id" => id}) do
    guitar = Instruments.get_guitar!(id)
    {:ok, _guitar} = Instruments.delete_guitar(guitar)

    conn
    |> put_flash(:info, "Guitar deleted successfully.")
    |> redirect(to: Routes.guitar_path(conn, :index))
  end
end
