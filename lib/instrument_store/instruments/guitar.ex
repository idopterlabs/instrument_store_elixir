defmodule InstrumentStore.Instruments.Guitar do
  use Ecto.Schema
  import Ecto.Changeset

  schema "guitars" do
    field :is_available, :boolean, default: false
    field :model, :string
    field :year, :integer

    timestamps()
  end

  @doc false
  def changeset(guitar, attrs) do
    guitar
    |> cast(attrs, [:model, :year, :is_available])
    |> validate_required([:model, :year, :is_available])
  end
end
